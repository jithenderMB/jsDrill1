function carsManufactureYears(data){
    if (typeof data === 'object'){ 
        
        //allCarsYears empty array created to store cars years
        const allCarsYears = [];
        for (let i=0;i<data.length;i++){
            let user = data[i];
            allCarsYears.push(user.car_year);
            
        }
        return allCarsYears;
    }
    else{
        return 'Enter a valid input';
    }
}

module.exports = carsManufactureYears;



// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
