function getCar33Details(data,userId){
    if (data === undefined || userId === undefined || data.length === 0 || typeof userId !== 'number' || typeof data !== 'object') return [];
    for (let i=0;i<data.length;i++){
        let user = data[i];
        if (user.id === userId){
            return user;
        }
    }
}


module.exports =  getCar33Details;

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"
